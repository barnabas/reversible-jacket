# This is the combined frontend-backend dockerfile for production.

# Builder: install packages and copy files
FROM node:16-alpine as build

RUN npm install -g pnpm
WORKDIR /usr/backend
COPY ./backend/pnpm-lock.yaml ./
RUN pnpm fetch
COPY ./backend ./
RUN pnpm install --offline
RUN pnpm build && pnpm prune --prod

WORKDIR /usr/frontend
COPY ./frontend/pnpm-lock.yaml ./
RUN pnpm fetch
COPY ./frontend ./
RUN pnpm install --offline
RUN pnpm build

# Copy files from builders to distroless runtime
FROM gcr.io/distroless/nodejs:16

WORKDIR /usr/app
COPY --from=build /usr/backend ./
COPY --from=build /usr/frontend/dist ./public
USER 1000
EXPOSE 3000
CMD ["node", "index.js"]