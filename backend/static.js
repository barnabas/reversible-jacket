const sirv = require("sirv");

/** This function sets up the static asset handlers.
 * For files that are in the public/assets directory, we want long expiration headers. But for
 * files in public (the shell html file), we don't want any caching at all.
 * The solution below is from here: https://github.com/lukeed/sirv/issues/113
 */
function configureStatic(app) {
  const assetsHandler = sirv("public/assets", {
    maxAge: 31536000,
    immutable: true,
  });

  const publicHandler = sirv("public", {
    single: true,
    setHeaders(res) {
      res.setHeader("Cache-Control", "no-cache");
    },
  });

  app.use("/assets", assetsHandler);
  app.use(publicHandler);
}

module.exports = {
  configureStatic,
};
