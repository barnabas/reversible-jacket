const express = require("express");
const { configureAPI } = require("./api");
const { configureStatic } = require("./static");

const app = express();
const port = process.env.PORT || 3001;

configureAPI(app);
configureStatic(app);

app.listen(port, "::", () => {
  console.log(`Listening on port ${port}`);
});
