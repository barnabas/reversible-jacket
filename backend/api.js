function configureAPI(app) {
  // define API endpoints here
  app.get("/api/now", function (req, res) {
    const d = new Date();
    const time = d.toLocaleTimeString();
    const date = d.toLocaleDateString();
    const timestamp = d.getTime();
    res.json({ date, time, timestamp });
  });

  app.get("/api/echo", function (req, res) {
    const { headers } = req;
    res.json({ headers });
  });
}

module.exports = {
  configureAPI,
};
